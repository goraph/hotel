﻿function goto(location) {
 var s;
 s='';
 next_move();

 if (location=='trup_look') {
   s = "The dead naked woman covered in blood. Blood slowly drips from her slit throat, dirtying everything around.<br>";
 }
 /*
 if (location=='trup_move') {
   if (room1_center==0) s = "Винсент взял девушку за руку и стянул её с кровати. Труп глухо упал в центре комнаты, оставив большой кровавый след на полу.<br>";
   else s = "Труп уже и так в центре комнаты, и весь пол комнаты в крови.<br>";
   room1_center=1;
 } */
 if (location=='trup_sluga') {
   s = "— Are you sure you don't remember what happened last night, sir? — Brooks asked. It seemed that he was not too pleased with current situation.<br>— I already said that I didn't do anything, Brooks, — the baron replied irritably. — It would be better if you could figure out what to do with this.<br>";
 }
 if (location=='trup_take') {
   trup=1;
   bedroom_blood=1;
   s = "— Help me, Brooks — said Vincent taking woman's hands. — Let's move her.<br>— You know, sir, I can't bear even the sight of blood.<br>— You'll have to bear, if you don't want us to be found with corpse. Hurry, take the legs!<br>— Of course, sir, — Brooks gritted his teeth and replied, taking woman's legs. <br>The blood from the woman's slit throat slowly dripped onto the floor, dirtying it.<br>";
 }

 if (location=='shkaf_look') {
   s = "This wardrobe was made in early nineteenth century by unknown artist. Massive classic wardrobe made of expensive ebony. Very old wardrobe with a few scratches on the doors and creaky hinges. It is possible that at one time it was standing in the Palace of Napoleon. And the young courtesans used it to store their countless dresses. It is not known how the owner of the hotel in Burgundy got it after Revolution. Anyway, the wardrobe is the most common and no need to talk about it more detailed.<br>";
 }
 if (location=='shkaf_open') {
   shkaf_open=1;
   s = "Vincent opened the wardrobe.<br>";
 }
 if (location=='shkaf_close') {
   shkaf_open=0;
   s = "Vincent closed the wardrobe.<br>";
 }
 if (location=='shkaf_knife') {
   knife=2;
   knife_hidden=1;
   s+="Vincent hid the bloody knife in the wardrobe. Anyone will hardly look for the knife in the wardrobe.<br>";
   s+=showscore(3);
 }
 if (location=='shkaf_trup') {
   trup=2;
   trup_hidden=1;
   trup_shkaf=1;
   s+="— Hurry, Brooks, — commanded Vincent putting corpse into the wardrobe. <br>Finally, the baron managed to stick it inside. Brooks helped to put woman's legs in. Vincent tightly closed the wardrobe and wiped the sweat from his forehead. One less thing to worry about.<br>";
   s+=showscore(5);
 }
 if (location=='shkaf_clothes') {
   clothes=-1;
   s+="Vincent put bloody clothes in the wardrobe. Obviously, the clothes will not be looked for in the wardrobe.<br>";
 }
 if (location=='shkaf_polotence') {
   polotence=-1;
   s+="Vincent hid maroon from the blood towel into the wardrobe. No one will find it now, even Vincent.<br>";
 }
 if (location=='shkaf_kover') {
   kover=-1;
   s+="Vincent shoved the blood stained carpet into the wardrobe. <br> — What the spacious wardrobe, Brooks. — said Vincent.<br>";
 }
 if (location=='shkaf_shtora') {
   shtora=-1;
   s+="After shoving bloody curtain into the wardrobe, Vincent sighed. The curtain is safe here away from the heavy baron's character.<br>";
 }
 if (location=='shkaf_bloodiebelie') {
   bloodie_belie=-1;
   s+="Hiding blood-stained sheets into the wardrobe, Vincent looked down grimly at Brooks:<br>— Hope, no one will find our dirty sheets.<br>";
 }

 if (location=='costume_look') {
   s+="Neat suit. Snow-white shirt and smooth ironed pants.<br>";
 }
 if (location=='costume_wear') {
   if (baron_blood==1) {s+="— Let me to notice, sir, — suddenly Brooks said. — You're covered in blood. If you put on new suit now, it will be dirty. You should take a bath first. <br>Nothing to say. Brooks was right. Vincent only frowned.<br>";
   }else {
     costume=1;
     s+="Vincent put on suit.<br>";
   }
 }
 if (location=='costume_sluga') {
   s+="— The suit fits you perfectly, sir, — said Brooks.<br>";
 }


 if (location=='knife_look') {
   if (knife_blood==1) {
    s = "A sharp knife, covered with blood.<br>";
    if (knife==1) s+=' There are Vincent\'s fingerprints on the blade.<br>';
   } else s = "The usual kitchen knife, it does not resemble a murder weapon.<br>";
 }
 if (location=='knife_take') {
   s = "Vincent took the knife.<br>— There are your fingerprints on it, sir, — notice Brooks.<br>";
   knife=1;
 }
 if (location=='knife_sluga') {
   s = "— What do you think about it, Brooks?<br>— I dare to say, sir, that knife is the murder weapon.<br>";
 }

 if (location=='shtora_look') {
   s+="The golden curtain looks quite rich and unusual for this room.<br>";
   if (shtora_blood==1) s+=" There are traces of blood on the curtain.<br>";
 }
 if (location=='shtora_open') {
   s+="Vincent tried to draw the curtain, but it didn't work. Probably, he is simply not created for such work.<br>";
 }
 if (location=='shtora_sluga') {
   shtora=1;
   s+="— Curtain, Brooks.<br>— As you command, sir, — the butler answered and pushed back the curtain. The bright sunlight entered the room from window. <br>";
 }
 if (location=='shtora_take') {
   shtora=2;
   s+="Vincent hung on the curtain and ripped it off. Sometimes the excess weight is the access weight.<br>";
 }
 if (location=='shtora_sluga2') {
   s+="— Beautiful curtain, sir, — said Brooks, looking closely at the curtain.<br>";
 }

 if (location=='window_look') {
   s+="The dirty window offers a beautiful view of the surrounding area. Puddles, the dirt road and a garbage dump in the distance.<br>";
 }
 if (location=='window_open') {
   s+="Vincent opened the window, but it becomes windy, so the baron, for fear of drafts, closed it back.<br>";
 }
 if (location=='window_sluga') {
   s+="— You right, sir, — agreed Brooks. — The window need to be cleaned. <br>And then he began to wipe it with his handkerchief. Very timely.<br>";
 }
 if (location=='window_knife') {
   knife=2;
   knife_hidden=1;
   s+="Without thinking, Vincent threw the knife out the window. The murder weapon disappeared without a trace. Unless, of course, somebody would guess to look at the grass under the window.<br>";
   s+=showscore(0);
 }
 if (location=='window_trup') {
   trup=2;
   trup_hidden=1;
   s+="— Now then, Brooks, — Vincent commands.<br>And baron with butler threw the corpse of a young woman out the window. Naked corpse fell from the second floor down, right in a puddle in front of the hotel<br>— When they will be looking at the room, no one would think to look for a body here, Brooks, — said Vincent.<br>— Certainly, sir, — agreed butler. — Here it can be found only by accident.<br>";
   s+=showscore(0);
 }
 if (location=='window_clothes') {
   clothes=-1;
   s+="Vincent threw the bloody clothes out the window, and it fell into a puddle with a soft splash. No one will find it here.<br>";
 }
 if (location=='window_polotence') {
   polotence=-1;
   s+="Vincent threw the dirty towel out the window. It fell right on the grass under the window.<br>";
 }
 if (location=='window_kover') {
   kover=-1;
   s+="Hardly throwing the carpet out the window, Vincent took a breath. Just a little bit further.<br>";
 }
 if (location=='window_shtora') {
   shtora=-1;
   s+="Vincent rolled the curtain and threw it out the window.<br>";
 }
 if (location=='window_bloodiebelie') {
   bloodie_belie=-1;
   s+="Vincent quietly threw away the bloody sheets.<br>";
 }

 if (location=='bed_look') {
   if (zasteleno!=1) s+="The bed would look welcoming, comfortable and inviting if it wasn't covered with blood.";
   if (trup==0) s+=" And decorated with a dead naked woman.<br>";
   if (zasteleno==1) s+=" The bed looks welcoming, comfortable and inviting, nothing resembling the outcome of last night.<br>";
 }
 if (location=='bed_sluga') {
   if (zasteleno!=1) s+="— Awful, sir, — Brooks paled.<br>";
   if (zasteleno==1) s+="— Looks great, sir. — Brooks noted.<br>";
 }
 if (location=='bed_knife') {
   knife=2;
   knife_hidden=1;
   s+="Vincent threw the knife under the bed. The murder weapon disappeared without a trace. <br>";
   s+=showscore(1);
 }
 if (location=='bed_shtora') {
   shtora=3;
   zasteleno=1;
   s+="— Now I'll show you the secret of how to make blood stains disappear, sir, — said Brooks, taking the curtain from Vincent.<br>The Butler waved the curtain, covering the bloody bed, and suddenly, everything becomes clean and decent.<br>— Wow, — surprised Vincent. — It's a miracle.<br>";
   s+=showscore(2);
 }
 if (location=='bed_kover') {
   kover=2;
   zasteleno=1;
   s+="— Now I'll show you a little secret, sir, — said Brooks, taking the carpet from Vincent.<br>He waved the carpet, covering the bloody bed, and suddenly, everything becomes clean and decent<br>— Wow, — surprised Vincent. — It's a miracle.<br>";
   s+=showscore(1);
 }
 if (location=='bed_belie') {
   belie=2;
   zasteleno=1;
   bloodie_belie=1;
   s+="Vincent quickly pulled off the bloodied bed sheets.<br > — I\'ll help you now, sir, — Brooks said, making the bed with clean linens. As if there were no consequences of the stormy night.<br>";
   s+=showscore(3);
 }
 if (location=='bed_clothes') {
   clothes=-1;
   s+="Vincent hid the bloody clothes under the bed.<br>";
 }
 if (location=='bed_polotence') {
   polotence=-1;
   s+="Vincent threw the dirty towel under the bed.<br>";
 }
 if (location=='bed_kover2') {
   kover=-1;
   s+="Vincent shoved the carpet under the bed.<br>";
 }
 if (location=='bed_shtora2') {
   shtora=-1;
   s+="Vincent hid the dirty curtain under the bed.<br>";
 }
 if (location=='bed_bloodiebelie') {
   bloodie_belie=-1;
   s+="Vincent hid the bloody sheets under the bed.<br>";
 }

 if (location=='geran_look') {
   s+="Blooming lily with white petals of different shades. The pot is quite large and massive.<br>";
 }
 if (location=='geran_smell') {
   s+="Delicate, exquisitely sweet floral scent, reminding rose or mint and blowing in the spring and natural tranquility. Even in such a uneasy situation.<br>";
 }
 if (location=='geran_lick') {
   s+="It tastes like regular lily. Same as in the other hotels.<br>";
 }
 if (location=='geran_sluga') {
   s+="— A magnificent flower, sir, — Brooks remarked. - Symbol of France. They say its smell is a great antidepressant, uplifting and relieves stress.<br>";
 }
 if (location=='geran_knife') {
   knife=2;
   knife_hidden=1;
   s+="Vincent dug a small hole in the ground, put a knife in it, and then covered it with earth. It is hidden safely enough here. Hardly anyone would look for a terrible murder weapon in a pot with a beautiful lily.<br>";
   s+=showscore(4);
 }

// ВАННАЯ
 if (location=='shtorka_look') {
   s = "Green shower curtain.<br>";
 }
 if (location=='shtorka_open') {
   bathroom_shtorka=1;
   s = "Vincent opened the curtain and found the bathtub.<br>";
 }
 if (location=='shtorka_close') {
   bathroom_shtorka=0;
   s = "Vincent closed the curtain.<br>";
 }

 if (location=='vannaya_look') {
   s = "The usual bathtub, has already been filled with hot water, with a thick foam, specially prepared by Brooks.<br>";
 }
 if (location=='vannaya_use') {
   if (baron_blood==1) {
    baron_blood=0;
    baron_wet=1;
    if (clothes!=-1) clothes=0;
    s='';
    if (trup_in_bathroom==1) s+="There is dead naked woman in the bathtub, but Vincent decided that it doesn't stop him from taking a good bath.<br>";
    if (knife_in_bathroom!=1) {
     if (clothes!=-1) s += "Vincent quickly undressed and dived into the bath. Now he is absolutely clean.<br>";
     else s += "Vincent dived in bath. Now he is absolutely clean.<br>";
   }  else s += "Vincent threw off his clothes and jumped into the bathtub, but immediately stepped on the knife! And only miraculously didn’t cut off anything! Cursing, Vincent still washed, and got back, though wet, but clean.<br>";
   } else {
    s+="Vincent has already clean enough. There is no time to take a bath several times.<br>";
   }
 }
 if (location=='vannaya_knife') {
   knife=2;
   knife_hidden=1;
   knife_in_bathroom=1;
   s+="Vincent threw the knife in the bathtub. The knife immediately went down.<br>";
   s+=showscore(2);
 }
 if (location=='vannaya_trup') {
   trup=2;
   trup_hidden=1;
   trup_in_bathroom=1;
   s+="The baron and the butler put the corpse into the bathtub. It basically went down, but the rest of it was hidden among the thick foam prepared by the Brooks.<br>";
   s+=showscore(3);
 }
 if (location=='vannaya_clothes') {
   clothes=-1;
   s+="Vincent put the bloody clothes into the bathtub.<br>";
 }
 if (location=='vannaya_polotence') {
   polotence=-1;
   s+="Vincent threw the dirty towel into the bathtub.<br>";
 }
 if (location=='vannaya_kover') {
   kover=-1;
   s+="Vincent threw the carpet in the bathtub.<br>";
 }
 if (location=='vannaya_shtora') {
   shtora=-1;
   s+="Vincent hid the curtain into the bathtub.<br>";
 }
 if (location=='vannaya_bloodiebelie') {
   bloodie_belie=-1;
   s+="Vincent put the bloody sheets into the bathtub.<br>";
 }

 if (location=='umivalnik_look') {
   s = "Ordinary washbasin.<br>";
 }
 if (location=='umivalnik_use') {
   if (baron_blood==1) s = "Vincent opened hot water and tried to wash himself. But no luck, there are too much blood on him. To completely clean, he need something more serious than a small washbasin.<br>";
    else s = "Vincent's hands have already clean.<br>";
 }
 if (location=='umivalnik_knife') {
   knife=2;
   knife_hidden=1;
   s+="Vincent washed the bloody knife with water, and now it doesn't look like a murder weapon. It looks like a regular table knife. One of many. It is useless to it. Brooks took the knife and carried it into the hall.<br>";
   s+=showscore(5);
 }

 if (location=='polotence_look') {
   if (polotence_blood==0) s = "Snow-white towel.<br>"; else s = "A towel covered in the blood.<br>";
 }
 if (location=='polotence_use') {
   if (polotence_blood==0) {
     s = "Vincent dry himself clean with a towel."; 
     if (baron_blood==1) {
        polotence_blood=1; s+=" Тhe towel is in the blood now, just like Vincent.<br>";
     } else if (baron_wet==0) {
        s+=" Although it didn't change anything, because Vincent wasn't wet at all.<br>";
     } else {
        baron_wet=0;
        s+=" Now Vincent is dry and clean.<br>";
     }
   }  else {
     if (costume==1) {
      s="Vincent looked at the towel, but it was all bloody and seemed too dirty. He didn't want it to use.<br>";
     } else {
      if (baron_blood==0) s = "Vincent dry himself with the bloody towel and now he's covered in blood.<br>";
       else s = "Vincent dry himself with the bloody towel. However, he and so was in blood. Nothing has changed.<br>";
      baron_blood=1;
     }
   }
 }
 if (location=='polotence_take') {
   if (polotence!=1) {
    polotence=1;
    polotence_blood=0;
    s+='Vincent took the towel.<br>';
   } else {
    s+='Vincent has already had a towel.<br>';
   }
 }
// ГОСТИНАЯ
 if (location=='servant_look') {
   s = "A cupboard where alcoholic drinks are stored.<br>";
 }
 if (location=='servant_open') {
   servant_open=1;
   s = "Vincent opened the cupboard.<br>";
 }
 if (location=='servant_close') {
   servant_open=0;
   s = "Vincent closed the cupboard.<br>";
 }
 if (location=='servant_knife') {
   knife=2;
   knife_hidden=1;
   s+="Vincent hid the bloody knife into the cupboard. It is very unlikely that anyone will find it here.<br>";
   s+=showscore(3);
 }

 if (location=='tumba_look') {
   s = "The cabinet, where bed linen is stored.";
   if (tumba_open==1) s+=" Opened.<br>"; else s+=' Closed.<br>';
 }
 if (location=='tumba_open') {
   tumba_open=1;
   s = "Vincent opened the cabinet.<br>";
 }
 if (location=='tumba_close') {
   tumba_open=0;
   s = "Vincent closed the cabinet.<br>";
 }
 if (location=='tumba_knife') {
   knife=2;
   knife_hidden=1;
   s+="Vincent hid the bloody knife into the cabinet. They'll not look for it here.<br>";
   s+=showscore(3);
 }
 if (location=='tumba_clothes') {
   clothes=-1;
   s+="Vincent hid the bloody clothes into the cabinet.<br>";
 }
 if (location=='tumba_polotence') {
   polotence=-1;
   s+="Vincent threw the dirty towel into the cabinet.<br>";
 }
 if (location=='tumba_bloodiebelie') {
   bloodie_belie=-1;
   s+="Vincent folded the bloody sheets into the cabinet.<br>";
 }

 if (location=='belie_look') {
   s = "Snow-white bed linen!<br>";
 }
 if (location=='belie_take') {
   s = "Vincent took the bed linen and straightened it, almost entangled in it. It looks too complicated for the baron, and Vincent has never really known how to use it. Better leave it to Brooks.<br>";
   belie=1;
 }
 if (location=='belie_sluga') {
   s = "Brooks took the bed linen, folded it neatly again, and put it back into the cabinet in the living room.<br>";
   belie=0;
   tumba_open=0;
 }

 if (location=='belie_look_blood') {
   s = "The sheets are covered in blood!<br>";
 }
 if (location=='belie_sluga_blood') {
   s = "— You know, I fear the sight of blood, sir, — Brooks backed away.<br>";
 }

 if (location=='whiskey_look') {                       
   s = "A bottle of fine Chablis. Unfortunately, empty.<br>";
 }
 if (location=='whiskey_take') {
   s = "Vincent took the empty bottle.<br>";
   whiskey_bottle=1;
 }
 if (location=='whiskey_sluga') {
   s = "— It was the crop of 1901, sir, — said Brooks.<br>";
 }
 if (location=='whiskey_give_sluga') {
   s = "— Better give me the bottle, sir, — Brooks said, taking the bottle from Vincent and putting it back on the table.<br>";
   whiskey_bottle=0;
 }
 if (location=='whiskey_drink') {
   s = "Unfortunately, the bottle is empty.<br>";
 }

 if (location=='kover_look') {
   s = "A striped-color carpet resembles a tiger, but who knows, from what it's really made. You can expect anything from these hotels.<br>";
   if (knife_hidden_kover==1) s+=" Under the carpet is clearly visible outline of the hidden knife.<br>";
 }
 if (location=='kover_take') {
   kover=1;
   s = "Vincent took the carpet.<br>";
   if (knife_hidden_kover==1) {
    knife=1;
    knife_hidden=0;
    knife_hidden_kover=0;
    s+=' Now the bloody knife, which was hidden under the carpet, becomes noticeable, because it lies just in the middle of the room. Vincent had to pick him up, too.<br>';
   }
 }
 if (location=='kover_knife') {
   knife=2;
   knife_hidden=1;
   knife_hidden_kover=1;
   s+="Vincent hid the bloody knife under the carpet. Now under the carpet there is a roughness, and it is noticeable. But, maybe no one will pay attention?<br>";
   s+=showscore(0);
 }
 if (location=='kover_sluga') {
   s = "— Lovely carpet, sir, — answered Brooks.<br>";
 }

 if (location=='door_look') {
   s = "Solid door. Locked from the inside. It will stand for some time, even if it is beaten, but sooner or later it will fall.<br>";
 }
 if (location=='door_open') {
   finish();
 }
 if (location=='door_sluga') {
   s = "— Not sure that it's a good idea, sir, — Brooks said standing still.<br>";
 }

// Брукс
 if (location=='sluga_look') {
   s = "Snow-white shirt, ironed suit and clean-shaven skull — Brooks, as always, is perfect.<br>";
 }
 if (location=='sluga_talk') {
   s = "— What do you think about this, Brooks?<br>— Complex story, sir, — he replied after some thought.<br>";
 }

 if (location=='clothes_look') {
   s = "Vincent's crumpled suit, where he fell asleep, shirt, pants, sleeves — all covered in the blood of the murdered woman.<br>";
 }
 if (location=='clothes_sluga') {
   s = "— What do you think about this, Brooks?<br>— You should change clothes, sir. — he replied after a short pause.<br>";
 }
 if (location=='clothes_sluga2') {
   s = "— What do you think of all this, Brooks?<br>— With your bloodstained clothes in your hands, you look rather suspicious, sir, — he replied with some thought.<br>";
 }
 if (location=='clothes_take') {
   clothes=2;
   s = "Vincent took the bloody clothes.<br>";
 }
 if (location=='clothes_takeoff') {
   clothes=2;
   s = "Vincent took off his bloody clothes.<br>";
 }
 if (location=='clothes_wear') {
   if (costume!=1) {
    clothes=1;
    s = "Vincent put on bloody clothes.";
    if (baron_blood==0) s+=" Now he's covered in blood again.<br>";
    baron_blood=1;
    s+='<br>';
   } else {
    s = "Vincent has already dressed in a clean and beautiful suit. There's no need to change.<br>";
   }
 }

 if (location=='blood_look') {
   s = "Traces of blood from the murdered woman.<br>";
 }
 if (location=='blood_clean') {
   s = "Vincent need something to clean the blood. He can't do it with bare hands.<br>";
 }
 if (location=='bedroom_blood_sluga') {
   s = "— You know, I'm afraid of the sight of blood, sir, — Brooks replied.<br>";
 }
 if (location=='bathroom_blood_sluga') {
   s = "— You know, I'm afraid of the sight of blood, sir, — Brooks replied.<br>";
 }
 if (location=='hall_blood_sluga') {
   s = "— You know, I'm afraid of the sight of blood, sir, — Brooks replied.<br>";
 }
 if (location=='bedroom_blood_polotence') {
   bedroom_blood=0;
   polotence_blood=1;
   s = "Vincent took the towel and cleaned the blood. The floor is clean now, but towel is all covered in blood.<br>";
 }
 if (location=='bathroom_blood_polotence') {
   bathroom_blood=0;
   polotence_blood=1;
   s = "Vincent took the towel and cleaned the blood. The bathroom's floor is now clean, but there are bloody marks on the towel.<br>";
 }
 if (location=='hall_blood_polotence') {
   hall_blood=0;
   polotence_blood=1;
   s = "Vincent took the towel and cleaned the blood. The floor is clean now, but there are blood traces on the towel.<br>";
 }
 if (location=='bedroom_blood_shtora') {
   bedroom_blood=0;
   shtora_blood=1;
   s = "Vincent took the curtain and clean the blood. The floor is now clean, but the curtain is all covered in blood.<br>";
 }
 if (location=='bathroom_blood_shtora') {
   bathroom_blood=0;
   shtora_blood=1;
   s = "Vincent took the curtain and clean the blood. The bathroom's floor is now clean, but the curtain is all covered in blood.<br>";
 }
 if (location=='hall_blood_shtora') {
   hall_blood=0;
   shtora_blood=1;
   s = "Vincent took the curtain and clean the blood in the hall. The floor is clean now, but the curtain is all covered in blood.<br>";
 }

 s+=each_turn();
 $('#response').html(s);
 move(current_location,true);
}
