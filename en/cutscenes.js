var games_saved_text='Game saved';
var no_saves_text='No saved games';
var game_restored_text='Game restored';


function intro() {
cutscene(`<div class="game_name">Vincent and Brooks</div> Baron Vincent La Montagne, travels with his butler Brooks. He could not even imagine the end of the night in a small Burgundy hotel. While the butler was checking in at the hotel and putting baggage into the room, Vincent managed to meet a pretty brunette Jacqueline in the bar. After a couple of bottles of wine and intimate flirt the baron with a companion went up to his room to spend the rest of the night with pleasure.
When the baron woke up in the morning after a stormy night, he found with a horror the dead Jacqueline\'s body with a slit throat. The whole bed was covered with blood. Vincent jumped out of bed and yelled:<br>
— Broooks!<br>
— Well, sir, — Brooks said when he entered the bedroom, thereby expressing the highest degree of wonder.<br>
— It\'s not what you think, Brooks, — said Vincent. — I just have spent the night with her.<br>
— I see, sir.<br>
— The girl was already dead when I woke up!<br>
— Maybe, you don\'t remember, how it happened, sir?<br>
— I remember everything perfectly! I only had four bottles of Chablis, and I was completely sober! <br>
Brooks looked suspiciously at the baron and the scene in the bedroom, but restrained himself:<br>
— What are you going to do, sir?
`,"The baron is thinking");
//Initial
  clothes=1;
}

function each_turn() {
 var s='';
 if (move_count==4) s+="<hr>Suddenly there was a knock on the door. <br> Baron turned pale:<br> — Who's this, Brooks?<br>— I don't know, sir — answered Brooks.";
 if (move_count==5) s+="<hr>Knock on the door was stopped. Maybe, if you do not make noise, the uninvited guests will think that there is no one at home, and just leave?";
 if (move_count==7) s+="<hr>The door was knocked again. What an inappropriate persistence and a complete lack of tact!";
 if (move_count==9) s+="<hr>The knock on the door continued.<br>— Open up, police! — a loud voice came from behind the door. <br>— Let me guess, sir, but I think they came for you, — said Brooks. <br> — God damn it. — Vincent nervously looked around the room. — We're screwed.";
 if (move_count==12) s+="<hr>The knock on the door continued: <br>—This is the police. Open up the door, we've been getting a complaint from the guests! <br>— Perhaps, we shouldn't open the door now, sir? — said Brooks.<br>— Really, Brooks? — answer Vincent.";
 if (move_count==15) s+="<hr>The unbearable knock on the door continued.";
 if (move_count==19) s+="<hr>The door continued to shake from the loud kicks of the police.";
 if (move_count==22) s+="<hr>— Monsieur, open the door! Or we'll have to break down it! <br>— We need to hurry, sir. — told Brooks.";
 if (move_count==25) s+="<hr>— All right, we're breaking down the door, monsieur. — There were strong hits, like someone try to kick the door in.<br> — Wait a minute, — the hotel owner's voice appealed, — I've got the spare keys. I'll bring it now.";
 if (move_count==28) s+="<hr>The voice of the owner came from outside, discussing something with the policemen. Unfortunately, words were not heard.";
 if (move_count==31) s+="<hr>There is silence behind the door, can not hear any sound.";
 if (move_count==38) s+="<hr>There were footsteps outside the door. <br>— Here are the keys. <br>— Just open.";
 if (move_count==41) s+="<hr>There is a screeching the keyhole — someone is opening the door with a key.";
 if (move_count==43) s+="<hr>A bunch of keys tinkled outside the door.<br> — Oh man, the wrong key, wait, I'll find.";
 if (move_count==45) s+="<hr>The keys tinkled again:<br> — Found! Opening. ";
 if (move_count>45) {
  finish(true);
 }
 return s;
}


function finish(a=false) {
  $("#main_window").hide();
  $("#static_text").show();
  $("#right_columns").hide();
  $("#inventory").hide();
  $("#right_columns_mob").hide();
  $("#inventory_mob").hide();
  if (a) var s = "The door opened and two policemen burst into the room. Behind them stood the hotel's owner with the keys. <br>";
   else {
    var s = "Vincent opened the door and saw two policemen. Behind them is the owner of the hotel with the keys. <br>";
   }
  if (shkaf_open==1 || servant_open==1 || tumba_open==1) {
    s+='Brooks behind Vincent quickly closed the door of';
    if (shkaf_open==1) s+="the wardrobe";
    else if (servant_open==1) s+="the cupboard";
    else if (tumba_open==1) s+="the dresser";
    s+='. ';
  }
  var lose=0;
  if (trup_hidden==0) {lose=1;s+="After seeing the corpse, they don't listen to any explanation and arrest Vincent and Brooks.";}
  else if (baron_blood==1) {lose=1;s+="Seeing the bloodied Vincent, they do not ask questions, and put handcuffs on the baron and the butler and searched the room. Soon, they found the corpse of the girl.";}
  else if (knife_hidden==0) {lose=1;s+="Policemen notice a bloody knife, and they don't ask questions, and put handcuffs on Vincent and Brooks and searched the room. Soon, they found the corpse of the girl.";}
  else if (hall_blood==1) {lose=1;s+="After noticing traces of blood on the floor of the living room, policemen arrested Vincent and Brooks and searched the room. Soon, they found the corpse of the girl.";}
  else if (bathroom_blood==1) {lose=1;s+="Once inside and seeing the traces of blood on the bathroom floor, they put handcuffs on baron and butler and searched the room. Pretty quickly, they found the girl's body.";}
  else if (bedroom_blood==1) {lose=1;s+="Once inside and noticing traces of blood on the bedroom floor, they arrest the baron and the butler and search the room. Soon, they found the corpse of the girl.";}
  else if (clothes!=-1) {lose=1;s+="Finding bloodied Vincent's clothes, the policemen don't ask questions, and put handcuffs on the baron and the butler and searched the room. Soon, they found the corpse of the girl.";}
  else if (zasteleno==0) {lose=1;s+="After seeing the blood-soaked bed, the policemen arrested Vincent and Brooks and searched the room. Soon, they found the corpse of the girl.";}
  else if (polotence_blood==1 && polotence!=-1) {lose=1;s+="Noticing the bloody towel, policemen don't listening to the objections of the baron and search the room and found the corpse. Then Vincent and Brooks are arrested.";}
  else if (shtora_blood==1 && shtora!=-1) {lose=1;s+="Having seen the blood-stained curtain, police officers search room and find a corpse. Without listening to Vincent's explanations, they arrest the baron and the butler.";}
  else if (kover_blood==1 && kover!=-1) {lose=1;s+="Finding bloodstained carpet, policemen raided the room. Soon, they found the corpse of the girl. Then Vincent and Brooks are arrested.";}
  else if (bloodie_belie==1) {lose=1;s+="Noticing the bloody sheets, the policemen arrest the Baron and the Butler and search the room. Soon, they found the corpse of the girl.";}
  if (lose==1) s+='<br>— This is outrage, I will complain, I was framed! — Vincent screamed as he was dragged down the hotel stairs. Brooks sighed, not trying to calm down Baron. And only the owner of the hotel, standing at the edge of the stairs, somehow smiled slyly.<br><div class="game_over">GAME OVER</div>';
   else s+="<br>The policemen go into the room and carefully inspect the room, but don\'t find anything suspicious.<br>— We apologize for the concern, Monsieur, — the policeman finally says. — It seems, we are misinformed, and we disturbed you for nothing.<br>— But it\'s supposed to be dead here, should... — the hotel owner anxiously ran into the room and frantically looked around. — Where did you put it? I left it right here on the bed!<br>— So you put the body in the room, sir? — Brooks was surprised.<br> The policemen, who were about to leave, immediately returned and approached the owner of the hotel:<br> — You will have to answer a couple of our questions, Monsieur. <br>Broken hotel owner fell to his knees and wept:<br> — Yes! I killed her! I noticed my Jacqueline going to the room with this bastard! After waiting for everyone to fall asleep, I opened the room with a spare key and killed her with a kitchen knife!  <br>— So you called the police, you scoundrel! — Vincent was indignant. <br>After that the baron and the butler told what happened in the room and showed the hidden evidence. The police arrested the innkeeper for the murder of his wife. And soon Vincent and Brooks were able to continue their difficult journey to Leukerbad.<br><div class=\"game_over\">GAME COMPLETED</div>";
  if (lose!=1) s+='<br><br><div class="game_over1">You scored '+score+' point of 13 possible!</div>';
  if (score>=13) s+="<br><br><hr>— May I ask, sir? <br>— Sure, Brooks, — Vincent replied, leaning back in the seat of the train compartment. <br>— How did you know what to do? Have you done anything like this before? <br>Vincent frowned: <br>— You weren't in the room last night, Brooks, were you? Where have you been? <br>— I understand, sir, — Brooks looked away in embarrassment and stared silently out the window, behind which the lights of the night France flashed.";
  $("#static_text").html(s);
}

function showscore(v) {
 score=score+v;
 if (v==0) return "<br>[ Your score increased by "+v+" points. ]";
 if (v==1) return "<br>[ Your score increased by  "+v+" point. ]";
 if (v<5) return "<br>[ Your score increased by  "+v+" points. ]";
 return "<br>[ Your score increased by  "+v+" points. ]";
}

function authors() {
cutscene(`
<div class="credits">Game studio:<br><div class="gamest">GEE!</div> <br>
Authors:<br>
Goraph <br>
Enola<br><br>
Beta-tester:<br>
Wol4ik<br>
Akela<br><br>
Thanks for playing.
</div><br>
<Center>`,"Back to game");
}
