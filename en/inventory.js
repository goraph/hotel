function inventory() {
 var s='';  
 var first=0;
 if (costume==1) {
//   s+='<br>';
   first=1;
   s+=dropdown("suit");
   s+=choice("costume_look","Examine");
   s+=choice("costume_sluga","Show suit to Brooks");
   s+=dropdownend();  
   s+=" (worn) ";
 }
 if (clothes==1) {
//   s+='<br>';
   first=1;
   s+=dropdownblood("clothes",1);
   s+=choice("clothes_look","Examine");
   s+=choice("clothes_takeoff","Take off");
   s+=choice("clothes_sluga","Show clothes to Brooks");
   s+=dropdownend();  
   s+=" (worn)";
 }
 if (clothes==2) {
//   s+='<br>';
   first=1;
   s+=dropdownblood("clothes",1);
   s+=choice("clothes_look","Examine");
   s+=choice("clothes_wear","Put on");
   s+=choice("clothes_sluga2","Show clothes to Brooks");
   s+=dropdownend();
   s+="";
 }
 if (whiskey_bottle==1) {
   if (first==1) s+='<br>';
   first=1;
   s+=dropdown("bottle");
   s+=choice("whiskey_look","Examine");
   s+=choice("whiskey_drink","Drink");
   s+=choice("whiskey_sluga","Show bottle to Brooks");
   s+=dropdownend();
/*
    s+=`<br><span class="dropdown"><a href='#' class='dropdown-toggle' data-toggle="dropdown" id="dropdownMenuButton" data-hover="dropdown">Бутылка виски</a>.
     <ul class="dropdown-menu">
      <li><a class="dropdown-item" href="#" onclick="goto('whiskey_look');">Examine</a></li>
      <li><a class="dropdown-item" href="#" onclick="goto('whiskey_drink')">Выпить</a></li>
      <li><a class="dropdown-item" href="#" onclick="goto('whiskey_give_sluga')">Показать слуге</a></li>
     </ul>
    </span>`;
*/
 }
 if (belie==1) {
   if (first==1) s+='<br>';
   first=1;
   s+=dropdown("bed linen");
   s+=choice("belie_look","Examine");
   s+=choice("belie_sluga","Give bed linen to Brooks");
   s+=dropdownend();
 }
 if (bloodie_belie==1) {
   if (first==1) s+='<br>';
   first=1;
   s+=dropdownblood("bloody bed linen",1);
   s+=choice("belie_look_blood","Examine");
   s+=choice("belie_sluga_blood","Give bloody bed linen to Brooks");
   s+=dropdownend();
 }
 if (knife==1) {
   if (first==1) s+='<br>';
   first=1;
   s+=dropdownblood("knife",1);
   s+=choice("knife_look","Examine");
   s+=choice("knife_sluga","Show knife to Brooks");
   s+=dropdownend();
 }
 if (shtora==2) {
   if (first==1) s+='<br>';
   first=1;
   s+=dropdownblood("curtain",shtora_blood);
   s+=choice("shtora_look","Examine");
   s+=choice("shtora_sluga2","Show curtain to Brooks");
   s+=dropdownend();
 }
 if (trup==1) {
   if (first==1) s+='<br>';
   first=1;
   s+=dropdownblood("corpse",1);
   s+=choice("trup_look","Examine");
   s+=choice("trup_sluga","Ask Brooks");
   s+=dropdownend();
 }
 if (kover==1) {
   if (first==1) s+='<br>';
   first=1;
   s+=dropdownblood("carpet",kover_blood);
   s+=choice("kover_look","Examine");
   s+=choice("kover_sluga","Show carpet to Brooks");
   s+=dropdownend();
 }
 if (polotence==1) {
   if (first==1) s+='<br>';
   first=1;
   s+=dropdownblood("towel",polotence_blood);
   s+=choice("polotence_look","Examine");
   s+=choice("polotence_use","Dry yourself with the towel");
   s+=dropdownend();
 }
 var s1=`  <a class="inventory_button" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
   Inventory
  </a><div class="collapse" id="collapseExample">
  <div class="card card-body" width=100%>`+s+'  </div></div>';
 $('#inventory_mob').html(s1);

 s='<b>Inventory</b><br>'+s;
 $('#inventory').html(s);
}

