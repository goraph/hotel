function move(location,refresh=false) {
 var s=''; 
 if (!refresh) {
  next_move();
  $('#response').html('');
  $('#response').html(each_turn());
 }

 if (location=='bedroom') {
   $('#room_title').html('Bedroom');
   s+=`There is the `+dropdown("wardrobe");
   s+=choice("shkaf_look","Look");
   if (shkaf_open==0) s+=choice("shkaf_open","Open");
   if (shkaf_open==1) s+=choice("shkaf_close","Close");
   if (shkaf_open==1 && knife==1) s+=choice("shkaf_knife","Hide the knife");
   if (shkaf_open==1 && trup==1) s+=choice("shkaf_trup","Hide the corpse");
   if (shkaf_open==1 && clothes==2) s+=choice("shkaf_clothes","Hide the clothes");
   if (shkaf_open==1 && polotence==1 && polotence_blood==1) s+=choice("shkaf_polotence","Hide the towel");
   if (shkaf_open==1 && kover==1 && kover_blood==1) s+=choice("shkaf_kover","Hide the carpet");
   if (shkaf_open==1 && shtora==2 && shtora_blood==1) s+=choice("shkaf_shtora","Hide the curtain");
   if (shkaf_open==1 && bloodie_belie==1) s+=choice("shkaf_bloodiebelie","Hide the bloody sheets");
   s+=dropdownend(); 
	s+=" in the corner of the bedroom"
    
   if (shkaf_open==0) s+=". The wardrobe is closed. ";
   if (shkaf_open==1) {
      s+=". The wardrobe is opened. ";
      if (costume==0 && trup_shkaf==1) {
       s+="There are the clean "+dropdown("suit");
       s+=choice("costume_look","Look");
       s+=choice("costume_wear","Put on");
	   s+=dropdownend(); 
	   s+=" and dead naked "+tooltip("woman","No one will find her here. If the wardrobe will be closed, of course.")
	   s+=" inside the wardrobe";
	   }
	   if (costume==0 && trup_shkaf!=1){
	   s+="There is the clean "+dropdown("suit");
       s+=choice("costume_look","Look");
       s+=choice("costume_wear","Put on");
       s+=dropdownend();
	   s+=" inside the wardrobe"
      }   
	   if (costume!=0 && trup_shkaf==1){
	   s+="There is the dead naked "+tooltip("woman","No one will find her here. If the wardrobe will be closed, of course.")
	   s+=" inside the wardrobe";
      }
	  if (costume!=0 && trup_shkaf!=1) s+="The wardrobe is empty";
     s+=". ";
   }

   s+=`The `+dropdownblood("bed", !zasteleno);
   s+=choice("bed_look","Look");
   if (knife==1) s+=choice("bed_knife","Hide knife under the bed");
   if (clothes==2) s+=choice("bed_clothes","Hide clothes under the bed");
   if (polotence==1 && polotence_blood==1) s+=choice("bed_polotence","Hide towel under the bed");
   if (kover==1 && kover_blood==1) s+=choice("bed_kover2","Hide carpet under the bed");
   if (shtora==2 && shtora_blood==1) s+=choice("bed_shtora2","Hide curtain under the bed");
   if (bloodie_belie==1) s+=choice("bed_bloodiebelie","Hide blood-stained sheets under the bed");
   if (shtora==2 && shtora_blood==0 && kover!=2 && belie!=2 && (room1_center!=0 || trup>0) ) s+=choice("bed_shtora","Make the bed with curtain");
   if (kover==1 && kover_blood==0 && shtora!=3 && belie!=2 && (room1_center!=0 || trup>0) ) s+=choice("bed_kover","Make the bed with carpet");
   if (belie==1 && kover!=2 && shtora!=3 && (room1_center!=0 || trup>0) ) s+=choice("bed_belie","Make the bed with linen");
   s+=choice("bed_sluga","Ask Brooks");
   s+=dropdownend();
   
	if (shtora==3) {
//     s+=` застеленная красивым `+tooltip("покрывалом","При ближайшем рассмотрении покрывало напоминает бывшую штору.")+". ";
     s+=` covered with a beautiful blanket, resembling the former curtain. `;
   } else if (kover==2) {
//     s+=` застеленная красивым `+tooltip("покрывалом","При ближайшем рассмотрении покрывало напоминает бывший ковёр.")+". ";
     s+=` made up with a beautiful bedspread, resembling the former carpet. `;
   } else if (belie==2) {
     s+=` made up with the white sheets. `;
   } else {
//     s+=` с залитой кровью `+tooltip("постелью","Подушка вся в крови. Да и простынь с одеялом тоже.")+". ";
     s+=` is completely covered in blood. `;
   }
   if (trup==0) {
     if (room1_center==0) { 
	 s+="The dead "+dropdownblood("woman",1);
	 s+=choice("trup_look","Look");
     s+=choice("trup_take","Take the corpse");
     s+=choice("trup_sluga","Ask Brooks");
     s+=dropdownend();
	 s+=" lies on the bed"
	 s+='. ';
     } else if (room1_center!=0) {
	 s+="The dead "+dropdownblood("woman",1);	 
     s+=choice("trup_look","Look");
     s+=choice("trup_take","Take the corpse");
     s+=choice("trup_sluga","Ask Brooks");
     s+=dropdownend();
	 s+=" lies on the floor in the middle of the room"
	 s+='. ';
	 }
	
    
   }
   /*s+=choice("trup_move","Drag the corpse");*/
   
   if (knife==0) {
      s+="There is the "+dropdownblood("knife",1);
      s+=choice("knife_look","Look");
      s+=choice("knife_take","Take");
      s+=choice("knife_sluga","Show knife to Brooks");
      s+=dropdownend();
	  s+=" on the bed near the pillow"
      s+='. ';
   }
   if (shtora==0) {
      s+="The only window in the room is covered by the "+dropdown("curtain");
      s+=choice("shtora_look","Look");
      s+=choice("shtora_open","Draw the curtain");
      s+=choice("shtora_take","Tear away the curtain");
      s+=choice("shtora_sluga","Order Brooks to draw the curtain");
      s+=dropdownend();
	  s+=". "
   } else {
	  s+="The pot with a "+dropdown("lily");
      s+=choice("geran_look","Look");
      s+=choice("geran_smell","Smell");
      s+=choice("geran_lick","Lick");
	  s+=choice("geran_sluga", "Ask Brooks");
      if (knife==1) s+=choice("geran_knife","Hide knife into the pot");
      s+=dropdownend();
	  s+=" is on the windowsill"
      if (shtora==1) {
       s+=" behind the "+dropdown("curtain");
       s+=choice("shtora_look","Look");
       s+=choice("shtora_take","Tear away");
       s+=dropdownend();
	  }
	  
      s+=" near the "+dropdown("window");
      s+=choice("window_look","Look");
      s+=choice("window_open","Open");
	  s+=choice("window_sluga","Ask Brooks");
      if (knife==1) s+=choice("window_knife","Throw the knife out the window");
      if (trup==1) s+=choice("window_trup","Throw the corpse out the window");
      if (clothes==2) s+=choice("window_clothes","Throw the clothes out the window");
      if (polotence==1 && polotence_blood==1) s+=choice("window_polotence","Throw the towel out the window");
      if (kover==1 && kover_blood==1) s+=choice("window_kover","Throw the carpet out the window");
      if (shtora==2 && shtora_blood==1) s+=choice("window_shtora","Throw the curtain out the window.");
      if (bloodie_belie==1) s+=choice("window_bloodiebelie","Throw the bloody sheets out the window");
      s+=dropdownend();
    
      s+=". ";
	  
	} 
	     if (bedroom_blood==1) {
	  s+=" There are drops of "+dropdownblood("blood",1);
      s+=choice("blood_look","Look"); 
	  if (polotence!=1 && shtora!=2) s+=choice("blood_clean","Clean the blood");
      if (shtora==2) s+=choice("bedroom_blood_shtora","Clean the blood with the curtain");
      if (polotence==1) s+=choice("bedroom_blood_polotence", "Clean the blood with the towel");
      s+=choice("bedroom_blood_sluga","Order Brooks to clean the blood");
      s+=dropdownend();
	  s+=" on the bedroom's floor";
      s+=". ";
   }
   
 
     
//   s+='<br>'+tooltip("Брукс","Невозмутим, как и всегда.")+' спокойно взирает на происходящее.';
 }

 if (location=='bathroom') {
   $('#room_title').html('Bathroom');
   if (bathroom_shtorka==0) { s+=`The `+dropdown("curtain");
		s+=choice("shtorka_open","Open the curtain");
		s+=choice("shtorka_look","Look");
		s+=dropdownend();
		s+=` is hiding the massive bathtub`;
		s+=". ";
   } 
   else if (bathroom_shtorka==1) { 
   if (bathroom_shtorka==1) {
      s+=`There is the big `+dropdown("bathtub");
      s+=choice("vannaya_look","Look");
      s+=choice("vannaya_use","Take bath");
      if (knife==1) s+=choice("vannaya_knife","Hide the knife into the bathtub");
      if (trup==1) s+=choice("vannaya_trup","Hide the corpse into the bathtub");
      if (clothes==2) s+=choice("vannaya_clothes","Hide the clothes into the bathtub");
      if (polotence==1 && polotence_blood==1) s+=choice("vannaya_polotence","Hide the towel into the bathtub");
      if (kover==1 && kover_blood==1) s+=choice("vannaya_kover","Hide the carpet into the bathtub");
      if (shtora==2 && shtora_blood==1) s+=choice("vannaya_shtora","Hide the curtain into the bathtub");
      if (bloodie_belie==1) s+=choice("vannaya_bloodiebelie","Hide the bloody sheets into the bathtub");
      s+=dropdownend();
	  s+=` behind the `+dropdown("curtain");
		s+=choice("shtorka_close","Close the curtain");
		s+=choice("shtorka_look","Look");
		s+=dropdownend();
	  s+=". ";
   }
   }

//   if (polotence!=1) {
//     s+=`, а рядом с ним `+tooltip("вешалка","Натуральный дуб.")+` с `+dropdown("полотенцем");
//     s+=`, в шкафчике под ним храняться чистые `+dropdownblood("полотенца", polotence_blood);
    s+=` The drawer with the `+dropdown("towels");
		s+=choice("polotence_look","Look");
		s+=choice("polotence_take","Take");
//     s+=choice("polotence_use","Вытереться");
		s+=dropdownend();
	s+=` is under the `+dropdown("washbasin");
		s+=choice("umivalnik_look","Look");
		s+=choice("umivalnik_use","Wash your hands");
		if (knife==1) s+=choice("umivalnik_knife","Clean the knife");
		s+=dropdownend();
   s+=' in the corner of the bathroom.';
//   }
  if (bathroom_blood==1) {
      s+=" There are drops of "+dropdownblood("blood",1);
      s+=choice("blood_look","Look");
      if (polotence!=1 && shtora!=2) s+=choice("blood_clean","Clean the blood");
      if (shtora==2) s+=choice("bathroom_blood_shtora","Clean the blood with the curtain");
      if (polotence==1) s+=choice("bathroom_blood_polotence", "Clean the blood with the towel");
      s+=choice("bathroom_blood_sluga","Order Brooks to clean the blood");
      s+=dropdownend();
      s+=" on the bathroom's floor";
	  s+=". ";
   }

   if (clothes==0) {
     s+=' Blood-stained '+dropdownblood("clothes",1);
     s+=choice("clothes_look","Look");
     s+=choice("clothes_take","Take");
     s+=dropdownend();
     s+=' is laying on the floor. ';
   }
	
 }

 if (location=='hall') {
   $('#room_title').html('Hall');
//   s+='В центре комнаты стоит стол с белоснежной скатертью, а рядом с ним '+tooltip("два","Один из стульев неустойчив и покачивается.")+' '+tooltip("стула","На ножке второго из стульев видны отметины, оставленные диким животным или кем-то из предыдущих постояльцев.")+'.';
   s+='Table with a white tablecloth and a few chairs is in the center of the room.';
      if (whiskey_bottle==0) {
       s+=` There is the empty `+tooltip("bottle","A bottle of fine Chablis. Unfortunately, empty.");
/*       s+=choice("whiskey_look","Look");
       s+=choice("whiskey_take","Take");
       s+=choice("whiskey_sluga","Позвать Брукса");
       s+=dropdownend();
*/
       s+=' on the table. ';
      }

//   s+=`У стены расположился зелёный `+tooltip("диван","Кожаный зелёный диван видел и лучшие дни, о чём свидетельствовали многочисленные потёртости на его оббивке.")+'';
/*   s+=`а рядом `+dropdown("сервант");
   s+=choice("servant_look","Look");
   if (servant_open==0) s+=choice("servant_open","Открыть");
   if (servant_open==1) s+=choice("servant_close","Закрыть");
   if (servant_open==1 && knife==1) s+=choice("servant_knife","Спрятать нож в сервант");
   s+=dropdownend();
   if (servant_open==1) {
      if (whiskey_bottle==0) {
       s+=`, внутри которого видна бутылка `+dropdown("виски");
       s+=choice("whiskey_look","Look");
       s+=choice("whiskey_take","Take");
       s+=choice("whiskey_sluga","Позвать дворецкого");
       s+=dropdownend();
      }
   }*/
   s+=' The '+dropdown("cabinet");
   s+=choice("tumba_look","Look");
   if (tumba_open==0) s+=choice("tumba_open","Open");
   if (tumba_open==1) s+=choice("tumba_close","Close");
   if (tumba_open==1 && knife==1) s+=choice("tumba_knife","Hide knife");
   if (tumba_open==1 && clothes==2) s+=choice("tumba_clothes","Hide the clothes");
   if (tumba_open==1 && polotence==1 && polotence_blood==1) s+=choice("tumba_polotence","Hide the towel");
   if (tumba_open==1 && bloodie_belie==1) s+=choice("tumba_bloodiebelie","Hide the bloody sheets");
   s+=dropdownend();
   if (tumba_open==1) {
      if (belie==0) {
       s+=` with the bed `+dropdown("linen");
       s+=choice("belie_look","Look");
       s+=choice("belie_take","Take");
       s+=dropdownend();
      }
   }
   s+=" is near the green sofa in the corner of the room"
   s+=". "
   
   if (kover==0) {
     s+=' The striped '+dropdown("carpet");
     s+=choice("kover_look","Look");
     s+=choice("kover_take","Take");
     if (knife==1) s+=choice("kover_knife","Hide knife under the carpet");
     s+=dropdownend();
     s+=' is on the floor.';
   }
   s+=' Solid front '+dropdown("door");
   s+=choice("door_look","Look");
   s+=choice("door_open","Open");
   s+=choice("door_sluga","Order Brooks to open the door");
   s+=dropdownend();
   s+=" leading to the hotel's corridor.";
   if (hall_blood==1) {
      s+=" There are drops of "+dropdownblood("blood",1);
      s+=choice("blood_look","Look");
      if (polotence!=1 && shtora!=2) s+=choice("blood_clean","Clean the blood");
      if (shtora==2) s+=choice("hall_blood_shtora","Clean the blood with the curtain");
      if (polotence==1) s+=choice("hall_blood_polotence","Clean the blood with the towel");
      s+=choice("hall_blood_sluga","Order Brooks to clean the blood");
      s+=dropdownend();
      s+=" on the hall's floor";
	  s+=". ";
   }
 }

 s+=`<br>`+dropdown("Brooks");
 s+=choice("sluga_look","Look");
 s+=choice("sluga_talk","Talk");
 s+=dropdownend();
 if (trup!=1) {
  s+=', as usual, stands near.';
 } else {
  s+=' helps to carry a dead woman, holding her legs and trying to look away. Blood from the throat of the murdered woman dripping on the floor, leaving a trail of blood.';
  if (current_location=='hall') {
    hall_blood=1;
    if (kover==0) kover_blood=1;
  }
  if (current_location=='bedroom') bedroom_blood=1;
  if (current_location=='bathroom') bathroom_blood=1;
 }


 $('#room_description').html(s);
 $('[data-toggle="tooltip"]').tooltip(); 
 if (current_location=='bathroom') $('#bathroom_link').html(`<a href='#' onclick="move('bathroom')">Bathroom</a>`);
 if (current_location=='bedroom') $('#bedroom_link').html(`<a href='#' onclick="move('bedroom')">Bedroom</a>`);
 if (current_location=='hall') $('#hall_link').html(`<a href='#' onclick="move('hall')">Hall</a>`);

 if (current_location=='bathroom') $('#bathroom_link_mob').html(`<a href='#' onclick="move('bathroom')">Bathroom</a>`);
 if (current_location=='bedroom') $('#bedroom_link_mob').html(`<a href='#' onclick="move('bedroom')">Baedroom</a>`);
 if (current_location=='hall') $('#hall_link_mob').html(`<a href='#' onclick="move('hall')">Hall</a>`);

 current_location=location;
 if (current_location=='bathroom') $('#bathroom_link').html('Bathroom');
 if (current_location=='bedroom') $('#bedroom_link').html('Bedroom');
 if (current_location=='hall') $('#hall_link').html('Hall');

 if (current_location=='bathroom') $('#bathroom_link_mob').html('Batrhoom');
 if (current_location=='bedroom') $('#bedroom_link_mob').html('Bedroom');
 if (current_location=='hall') $('#hall_link_mob').html('Hall');
 inventory(); 

 $('.dropdown').mouseenter(function(){
    if(!$('.navbar-toggle').is(':visible')) { // disable for mobile view
        if(!$(this).hasClass('show')) { // Keeps it open when hover it again
          if (!touch_device)
            $('.dropdown-toggle-game', this).trigger('click');
        }
    }
 });

}

function refreshlinks() {
  $('#bathroom_link').html(`<a href='#' onclick="move('bathroom')">Bathroom</a>`);
  $('#bedroom_link').html(`<a href='#' onclick="move('bedroom')">Bedroom</a>`);
  $('#hall_link').html(`<a href='#' onclick="move('hall')">Hall</a>`);
  $('#bathroom_link_mob').html(`<a href='#' onclick="move('bathroom')">Bathroom</a>`);
  $('#bedroom_link_mob').html(`<a href='#' onclick="move('bedroom')">Bedroom</a>`);
  $('#hall_link_mob').html(`<a href='#' onclick="move('hall')">Hall</a>`);
  $('#move-counter').html(move_count);
}