var move_count=0;
var current_location='bedroom';
var score=0;

var room1_center=0;
var whiskey_bottle=0;
var shkaf_open=0;
var bathroom_shtorka=0;
var polotence_blood=0;
var baron_blood=1;
var baron_wet=0;
var servant_open=0;
var tumba_open=0;
var belie=0;
var knife=0;
var knife_blood=1;
var knife_hidden=0;
var knife_in_bathroom=0;
var knife_hidden_kover=0;
var shtora=0;
var clothes=0;
var kover=0;
var kover_blood=0;
var trup=0;
var trup_hidden=0;
var trup_in_bathroom=0;
var trup_shkaf=0;
var bedroom_blood=0;
var hall_blood=0;
var bathroom_blood=0;
var polotence=0;
var shtora_blood=0;
var costume=0;
var zasteleno=0;
var bloodie_belie=0;

var touch_device=false;

function next_move() {
 move_count++;
 $('#move-counter').html(move_count);
}


function showloc(location) {
 var s='';
 if (location=='room1') {
 }
}

function supportsLocalStorage() {
    return ('localStorage' in window) && window['localStorage'] !== null;
}

function showtoast(s) {
  var x = document.getElementById("snackbar");
  x.className = "show";
  $('#snackbar').html(s);
  setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}

function gamesave() {
    if (!supportsLocalStorage()) { alert("local storage is not supported");return false; }
 localStorage.move_count = move_count;
 localStorage.current_location = current_location;
 localStorage.room1_center = room1_center;
 localStorage.whiskey_bottle = whiskey_bottle;

 localStorage.move_count=move_count;
 localStorage.current_location=current_location;
 localStorage.score=score;

 localStorage.room1_center=room1_center;
 localStorage.whiskey_bottle=whiskey_bottle;
 localStorage.shkaf_open=shkaf_open;
 localStorage.bathroom_shtorka=bathroom_shtorka;
 localStorage.polotence_blood=polotence_blood;
 localStorage.baron_blood=baron_blood;
 localStorage.baron_wet=baron_wet;
 localStorage.servant_open=servant_open;
 localStorage.tumba_open=tumba_open;
 localStorage.belie=belie;
 localStorage.knife=knife;
 localStorage.knife_blood=knife_blood;
 localStorage.knife_hidden=knife_hidden;
 localStorage.knife_in_bathroom=knife_in_bathroom;
 localStorage.knife_hidden_kover=knife_hidden_kover;
 localStorage.shtora=shtora;
 localStorage.clothes=clothes;
 localStorage.kover=kover;
 localStorage.kover_blood=kover_blood;
 localStorage.trup=trup;
 localStorage.trup_hidden=trup_hidden;
 localStorage.trup_in_bathroom=trup_in_bathroom;
 localStorage.trup_shkaf=trup_shkaf;
 localStorage.bedroom_blood=bedroom_blood;
 localStorage.hall_blood=hall_blood;
 localStorage.bathroom_blood=bathroom_blood;
 localStorage.polotence=polotence;
 localStorage.shtora_blood=shtora_blood;
 localStorage.costume=costume;
 localStorage.zasteleno=zasteleno;
 localStorage.bloodie_belie=bloodie_belie;

 showtoast(games_saved_text);
}

function gameload() {
 if (!current_location) {
    showtoast(no_saves_text);
    return false;
 }
 move_count = localStorage.move_count;
 current_location = localStorage.current_location;
 room1_center = localStorage.room1_center;
 whiskey_bottle = localStorage.whiskey_bottle;

 move_count=localStorage.move_count;
 current_location=localStorage.current_location;
 score=localStorage.score;

 room1_center=localStorage.room1_center;
 whiskey_bottle=localStorage.whiskey_bottle;
 shkaf_open=localStorage.shkaf_open;
 bathroom_shtorka=localStorage.bathroom_shtorka;
 polotence_blood=localStorage.polotence_blood;
 baron_blood=localStorage.baron_blood;
 baron_wet=localStorage.baron_wet;
 servant_open=localStorage.servant_open;
 tumba_open=localStorage.tumba_open;
 belie=localStorage.belie;
 knife=localStorage.knife;
 knife_blood=localStorage.knife_blood;
 knife_hidden=localStorage.knife_hidden;
 knife_in_bathroom=localStorage.knife_in_bathroom;
 knife_hidden_kover=localStorage.knife_hidden_kover;
 shtora=localStorage.shtora;
 clothes=localStorage.clothes;
 kover=localStorage.kover;
 kover_blood=localStorage.kover_blood;
 trup=localStorage.trup;
 trup_hidden=localStorage.trup_hidden;
 trup_in_bathroom=localStorage.trup_in_bathroom;
 trup_shkaf=localStorage.trup_shkaf;
 bedroom_blood=localStorage.bedroom_blood;
 hall_blood=localStorage.hall_blood;
 bathroom_blood=localStorage.bathroom_blood;
 polotence=localStorage.polotence;
 shtora_blood=localStorage.shtora_blood;
 costume=localStorage.costume;
 zasteleno=localStorage.zasteleno;
 bloodie_belie=localStorage.bloodie_belie;

 $("#static_text").hide();
 $('#main_window').show();
 $('#right_columns').show();
 $('#right_columns_mob').show();
 $('#inventory').show();
 $('#inventory_mob').show();
 $('#move-counter').html(move_count);
  $("#main_window").css("visibility", "visible");
  $("#right_columns").css("visibility", "visible");
  $("#right_columns_mob").css("visibility", "visible");

  refreshlinks();

 showtoast(game_restored_text);
 move(current_location,true)
}

function gamerestart() {
 move_count=0;
 current_location='bedroom';
 score=0;

 room1_center=0;
 whiskey_bottle=0;
 shkaf_open=0;
 bathroom_shtorka=0;
 polotence_blood=0;
 baron_blood=1;
 baron_wet=0;
 servant_open=0;
 tumba_open=0;
 belie=0;
 knife=0;
 knife_blood=1;
 knife_hidden=0;
 knife_in_bathroom=0;
 knife_hidden_kover=0;
 shtora=0;
 clothes=0;
 kover=0;
 kover_blood=0;
 trup=0;
 trup_hidden=0;
 trup_in_bathroom=0;
 trup_shkaf=0;
 bedroom_blood=0;
 hall_blood=0;
 bathroom_blood=0;
 polotence=0;
 shtora_blood=0;
 costume=0;
 zasteleno=0;
 bloodie_belie=0;

  $("#main_window").hide();
  $("#right_columns").hide();
  $("#inventory").hide();
  $("#right_columns_mob").hide();
  $("#inventory_mob").hide();
  move("bedroom",true);
  $("static_text").hide();

  refreshlinks();
  intro();
}


function cutscene(s,n) {
  $("#main_window").hide();
  $("#static_text").show();
  $("#right_columns").hide();
  $("#inventory").hide();
  $("#right_columns_mob").hide();
  $("#inventory_mob").hide();
  $("#static_text").html(s+next(n));
}

function tooltip(s1,s2) {
  return `<a href='#' class="link-tooltip" data-toggle="tooltip" data-original-title="`+s2+`">`+s1+`</a>`;
}

function dropdownblood(s1,blooded) {
 if (blooded==1)
  return `<span class="dropdown blood"><a href='#' class='dropdown-toggle-game blood' data-toggle="dropdown" data-hover="dropdown">`+s1+`</a><ul class="dropdown-menu">`;
 else return dropdown(s1);
}


function dropdown(s1) {
 return `<span class="dropdown"><a href='#' class='dropdown-toggle-game' data-toggle="dropdown" data-hover="dropdown">`+s1+`</a><ul class="dropdown-menu">`;
}

function choice(s1,s2) {
 return  `<li><a class="dropdown-item" href="#" onclick="goto('`+s1+`');">`+s2+`</a></li>`;
}

function dropdownend() {
 return `</ul></span>`;
}

function next(s) {
 return `<br><a href="#" onclick='clicknext();'>`+s+`</a>`;
}

function clicknext() {
  $("#main_window").show();
  $("#static_text").hide();
  $("#right_columns").show(); 
  $("#inventory").show();
  $("#right_columns_mob").show(); 
  $("#inventory_mob").show();
  $("#main_window").css("visibility", "visible");
  $("#right_columns").css("visibility", "visible");
  $("#right_columns_mob").css("visibility", "visible");
  move(current_location,true);
}

$('.dropdown').mouseenter(function(){
    if(!$('.navbar-toggle').is(':visible')) { // disable for mobile view
        if(!$(this).hasClass('open')) { // Keeps it open when hover it again
          if (!touch_device)
            $('.dropdown-toggle-game', this).trigger('click');
        }
    }
});



$( document ).ready(function() {
//  move_count--;
  current_location="bedroom";
  move("bedroom",true);
  $("static_text").hide();
  intro();

if ("ontouchstart" in document.documentElement)
{
 touch_device=true;
}
else
{
 touch_device=true;
}

});
