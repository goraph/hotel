function move(location,refresh=false) {
 var s=''; 
 if (!refresh) {
  next_move();
  $('#response').html('');
  $('#response').html(each_turn());
 }

 if (location=='bedroom') {
   $('#room_title').html('Спальня');
   s+=`В углу спальни приютился массивный `+dropdown("шкаф");
   s+=choice("shkaf_look","Осмотреть");
   if (shkaf_open==0) s+=choice("shkaf_open","Открыть");
   if (shkaf_open==1) s+=choice("shkaf_close","Закрыть");
   if (shkaf_open==1 && knife==1) s+=choice("shkaf_knife","Спрятать нож в шкаф");
   if (shkaf_open==1 && trup==1) s+=choice("shkaf_trup","Спрятать труп в шкаф");
   if (shkaf_open==1 && clothes==2) s+=choice("shkaf_clothes","Спрятать одежду в шкаф");
   if (shkaf_open==1 && polotence==1 && polotence_blood==1) s+=choice("shkaf_polotence","Спрятать полотенце в шкаф");
   if (shkaf_open==1 && kover==1 && kover_blood==1) s+=choice("shkaf_kover","Спрятать ковёр в шкаф");
   if (shkaf_open==1 && shtora==2 && shtora_blood==1) s+=choice("shkaf_shtora","Спрятать штору в шкаф");
   if (shkaf_open==1 && bloodie_belie==1) s+=choice("shkaf_bloodiebelie","Спрятать окровавленные простыни");

   s+=dropdownend()
   if (shkaf_open==0) s+=". Шкаф закрыт. ";
   if (shkaf_open==1) {
      s+=". Шкаф открыт. Внутри шкафа";
      if (costume==0) {
       s+=" чистая "+dropdown("одежда");
       s+=choice("costume_look","Осмотреть");
       s+=choice("costume_wear","Надеть");
       s+=dropdownend();
      }
      if (trup_shkaf==1 && costume==0) s+=" и мёртвая голая "+tooltip("девушка","Здесь её никто не найдёт. Если закрыть дверцу шкафа, конечно.");
	  if (trup_shkaf==1 && costume!=0) s+=" мёртвая голая "+tooltip("девушка","Здесь её никто не найдёт. Если закрыть дверцу шкафа, конечно.");
      if (costume!=0 && trup_shkaf!=1) s+=" пусто";
      s+=". ";
   }

   s+=`В центре комнаты расположена большая `+dropdownblood("кровать", !zasteleno);
   s+=choice("bed_look","Осмотреть");
   if (knife==1) s+=choice("bed_knife","Спрятать нож под кровать");
   if (clothes==2) s+=choice("bed_clothes","Спрятать одежду под кровать");
   if (polotence==1 && polotence_blood==1) s+=choice("bed_polotence","Спрятать полотенце под кровать");
   if (kover==1 && kover_blood==1) s+=choice("bed_kover2","Спрятать ковёр под кровать");
   if (shtora==2 && shtora_blood==1) s+=choice("bed_shtora2","Спрятать штору под кровать");
   if (bloodie_belie==1) s+=choice("bed_bloodiebelie","Спрятать окровавленные простыни под кровать");
   if (shtora==2 && shtora_blood==0 && kover!=2 && belie!=2 && (room1_center!=0 || trup>0) ) s+=choice("bed_shtora","Накрыть кровать шторой");
   if (kover==1 && kover_blood==0 && shtora!=3 && belie!=2 && (room1_center!=0 || trup>0) ) s+=choice("bed_kover","Накрыть кровать ковром");
   if (belie==1 && kover!=2 && shtora!=3 && (room1_center!=0 || trup>0) ) s+=choice("bed_belie","Постелить чистое бельё");
   s+=choice("bed_sluga","Спросить мнение Брукса");
   s+=dropdownend();
   if (shtora==3) {
//     s+=` застеленная красивым `+tooltip("покрывалом","При ближайшем рассмотрении покрывало напоминает бывшую штору.")+". ";
     s+=` застеленная красивым покрывалом, напоминающим бывшую штору. `;
   } else if (kover==2) {
//     s+=` застеленная красивым `+tooltip("покрывалом","При ближайшем рассмотрении покрывало напоминает бывший ковёр.")+". ";
     s+=` застеленная красивым покрывалом, напоминающим бывший ковёр. `;
   } else if (belie==2) {
     s+=` с белоснежными простынями. `;
   } else {
//     s+=` с залитой кровью `+tooltip("постелью","Подушка вся в крови. Да и простынь с одеялом тоже.")+". ";
     s+=` с залитыми кровью простынями. `;
   }
   if (trup==0) {
     if (room1_center==0) s+="На кровати лежит тело мёртвой "+dropdownblood("девушки",1);
     if (room1_center!=0) s+="На полу, прямо посреди комнаты лежит тело мёртвой "+dropdownblood("девушки",1);
     s+=choice("trup_look","Осмотреть");
     s+=choice("trup_take","Перенести труп");
     s+=choice("trup_sluga","Спросить мнение Брукса");
     s+=dropdownend();
     s+='. ';
   }
   /*s+=choice("trup_move","Тащить труп");*/
   if (knife==0) {
      s+=" Рядом с телом убитой лежит окровавленный "+dropdownblood("нож",1);
      s+=choice("knife_look","Осмотреть");
      s+=choice("knife_take","Взять");
      s+=choice("knife_sluga","Показать нож Бруксу");
      s+=dropdownend();
      s+='. ';
   }
   if (shtora==0) {
      s+="Единственное окно в комнате закрыто "+dropdown("шторой");
      s+=choice("shtora_look","Осмотреть");
      s+=choice("shtora_open","Отодвинуть штору в сторону");
      s+=choice("shtora_take","Сорвать штору");
      s+=choice("shtora_sluga","Приказать Бруксу отодвинуть штору");
      s+=dropdownend();
	  s+=". "
   } else {
      s+="На подоконнике единственного в комнате "+dropdown("окна");
      s+=choice("window_look","Осмотреть");
      s+=choice("window_open","Открыть");
      if (knife==1) s+=choice("window_knife","Выбросить нож в окно");
      if (trup==1) s+=choice("window_trup","Выбросить труп в окно");
      if (clothes==2) s+=choice("window_clothes","Выбросить одежду в окно");
      if (polotence==1 && polotence_blood==1) s+=choice("window_polotence","Выбросить полотенце в окно");
      if (kover==1 && kover_blood==1) s+=choice("window_kover","Выбросить ковёр в окно");
      if (shtora==2 && shtora_blood==1) s+=choice("window_shtora","Выбросить штору в окно");
      if (bloodie_belie==1) s+=choice("window_bloodiebelie","Выбросить окровавленные простыни в окно");
      s+=choice("window_sluga","Спросить мнение Брукса");
      s+=dropdownend();
      s+=", стоит горшок с "+dropdown("геранью");
      s+=choice("geran_look","Осмотреть");
      s+=choice("geran_smell","Нюхать");
      s+=choice("geran_lick","Лизнуть");
      if (knife==1) s+=choice("geran_knife","Спрятать нож в горшок");
      s+=choice("geran_sluga","Спросить мнение Брукса");
      s+=dropdownend();
      if (shtora==1) {
       s+=". Рядом с окном висит "+dropdown("штора");
       s+=choice("shtora_look","Осмотреть");
       s+=choice("shtora_take","Сорвать");
       s+=dropdownend();
      }
      s+=". ";
   }
   if (bedroom_blood==1) {
      s+=" На полу видны следы "+dropdownblood("крови",1);
      s+=choice("blood_look","Осмотреть");
      if (polotence!=1 && shtora!=2) s+=choice("blood_clean","Стереть");
      if (shtora==2) s+=choice("bedroom_blood_shtora","Стереть кровь шторой");
      if (polotence==1) s+=choice("bedroom_blood_polotence","Стереть кровь полотенцем");
      s+=choice("bedroom_blood_sluga","Приказать Бруксу стереть кровь");
      s+=dropdownend();
      s+=". ";
   }
//   s+='<br>'+tooltip("Брукс","Невозмутим, как и всегда.")+' спокойно взирает на происходящее.';
 }

 if (location=='bathroom') {
   $('#room_title').html('Ванная');
   if (bathroom_shtorka==0) s+=`В углу скрывается ванна за задёрнутой `+dropdown("шторкой");
   if (bathroom_shtorka==1) s+=`За отдёрнутой `+dropdown("шторкой");
   s+=choice("shtorka_look","Осмотреть");
   if (bathroom_shtorka==0) s+=choice("shtorka_open","Отодвинуть");
   if (bathroom_shtorka==1) s+=choice("shtorka_close","Задвинуть");
   s+=dropdownend();
   if (bathroom_shtorka==1) {
      s+=` видна `+dropdown("ванна");
      s+=choice("vannaya_look","Осмотреть");
      s+=choice("vannaya_use","Помыться");
      if (knife==1) s+=choice("vannaya_knife","Утопить нож в ванне");
      if (trup==1) s+=choice("vannaya_trup","Утопить труп в ванне");
      if (clothes==2) s+=choice("vannaya_clothes","Утопить одежду в ванне");
      if (polotence==1 && polotence_blood==1) s+=choice("vannaya_polotence","Утопить полотенце в ванне");
      if (kover==1 && kover_blood==1) s+=choice("vannaya_kover","Утопить ковёр в ванне");
      if (shtora==2 && shtora_blood==1) s+=choice("vannaya_shtora","Утопить штору в ванне");
      if (bloodie_belie==1) s+=choice("vannaya_bloodiebelie","Утопить окровавленные простыни в ванне");
      s+=dropdownend();
   }
   s+=`. У стены находится `+dropdown("умывальник");
   s+=choice("umivalnik_look","Осмотреть");
   s+=choice("umivalnik_use","Вымыть руки");
   if (knife==1) s+=choice("umivalnik_knife","Вымыть нож");
   s+=dropdownend();
//   if (polotence!=1) {
//     s+=`, а рядом с ним `+tooltip("вешалка","Натуральный дуб.")+` с `+dropdown("полотенцем");
//     s+=`, в шкафчике под ним храняться чистые `+dropdownblood("полотенца", polotence_blood);
     s+=`, в шкафчике под ним храняться чистые `+dropdown("полотенца");
     s+=choice("polotence_look","Осмотреть");
//     s+=choice("polotence_use","Вытереться");
     s+=choice("polotence_take","Взять");
     s+=dropdownend();
//   }
   s+='. ';
   if (clothes==0) {
     s+=' Прямо на полу лежит ваша окровавленная '+dropdownblood("одежда",1);
     s+=choice("clothes_look","Осмотреть");
     s+=choice("clothes_take","Взять");
     s+=dropdownend();
     s+='. ';
   }
   if (bathroom_blood==1) {
      s+=" На полу видны следы "+dropdownblood("крови",1);
      s+=choice("blood_look","Осмотреть");
      if (polotence!=1 && shtora!=2) s+=choice("blood_clean","Стереть");
      if (shtora==2) s+=choice("bathroom_blood_shtora","Стереть кровь шторой");
      if (polotence==1) s+=choice("bathroom_blood_polotence","Стереть кровь полотенцем");
      s+=choice("bathroom_blood_sluga","Приказать Бруксу стереть кровь");
      s+=dropdownend();
      s+=". ";
   }

 }

 if (location=='hall') {
   $('#room_title').html('Гостиная');
//   s+='В центре комнаты стоит стол с белоснежной скатертью, а рядом с ним '+tooltip("два","Один из стульев неустойчив и покачивается.")+' '+tooltip("стула","На ножке второго из стульев видны отметины, оставленные диким животным или кем-то из предыдущих постояльцев.")+'.';
   s+='В центре комнаты стоит стол с белоснежной скатертью и несколько стульев, а у стены зелёный диван.';
      if (whiskey_bottle==0) {
       s+=` На столе лежит пустая `+tooltip("бутылка","Бутылка прекрасного Шабли. К сожалению, пустая.");
/*       s+=choice("whiskey_look","Осмотреть");
       s+=choice("whiskey_take","Взять");
       s+=choice("whiskey_sluga","Позвать Брукса");
       s+=dropdownend();
*/
       s+='. ';
      }

//   s+=`У стены расположился зелёный `+tooltip("диван","Кожаный зелёный диван видел и лучшие дни, о чём свидетельствовали многочисленные потёртости на его оббивке.")+'';
/*   s+=`а рядом `+dropdown("сервант");
   s+=choice("servant_look","Осмотреть");
   if (servant_open==0) s+=choice("servant_open","Открыть");
   if (servant_open==1) s+=choice("servant_close","Закрыть");
   if (servant_open==1 && knife==1) s+=choice("servant_knife","Спрятать нож в сервант");
   s+=dropdownend();
   if (servant_open==1) {
      if (whiskey_bottle==0) {
       s+=`, внутри которого видна бутылка `+dropdown("виски");
       s+=choice("whiskey_look","Осмотреть");
       s+=choice("whiskey_take","Взять");
       s+=choice("whiskey_sluga","Позвать дворецкого");
       s+=dropdownend();
      }
   }*/
   s+=' У противоположной стены находится '+dropdown("тумбочка");
   s+=choice("tumba_look","Осмотреть");
   if (tumba_open==0) s+=choice("tumba_open","Открыть");
   if (tumba_open==1) s+=choice("tumba_close","Закрыть");
   if (tumba_open==1 && knife==1) s+=choice("tumba_knife","Спрятать нож в тумбочку");
   if (tumba_open==1 && clothes==2) s+=choice("tumba_clothes","Спрятать одежду в тумбочку");
   if (tumba_open==1 && polotence==1 && polotence_blood==1) s+=choice("tumba_polotence","Спрятать полотенце в тумбочку");
   if (tumba_open==1 && bloodie_belie==1) s+=choice("tumba_bloodiebelie","Спрятать окровавленные простыни в тумбочку");
   s+=dropdownend();
   if (tumba_open==1) {
      if (belie==0) {
       s+=`, внутри которой находится постельное `+dropdown("бельё");
       s+=choice("belie_look","Осмотреть");
       s+=choice("belie_take","Взять");
       s+=dropdownend();
      }
   }
   if (kover==0) {
     s+='. На полу лежит полосатый '+dropdown("ковёр");
     s+=choice("kover_look","Осмотреть");
     s+=choice("kover_take","Взять");
     if (knife==1) s+=choice("kover_knife","Спрятать нож под ковёр");
     s+=dropdownend();
   }
   s+='. Крепкая входная '+dropdown("дверь");
   s+=choice("door_look","Осмотреть");
   s+=choice("door_open","Открыть");
   s+=choice("door_sluga","Приказать Бруксу открыть дверь");
   s+=dropdownend();
   s+=' ведёт наружу.';
   if (hall_blood==1) {
      s+=" На полу видны следы "+dropdownblood("крови",1);
      s+=choice("blood_look","Осмотреть");
      if (polotence!=1 && shtora!=2) s+=choice("blood_clean","Стереть");
      if (shtora==2) s+=choice("hatt_blood_shtora","Стереть кровь шторой");
      if (polotence==1) s+=choice("hall_blood_polotence","Стереть кровь полотенцем");
      s+=choice("hall_blood_sluga","Приказать Бруксу стереть кровь");
      s+=dropdownend();
      s+=". ";
   }
 }

 s+=`<br>`+dropdown("Брукс");
 s+=choice("sluga_look","Осмотреть");
 s+=choice("sluga_talk","Говорить");
 s+=dropdownend();
 if (trup!=1) {
  s+=', как всегда, стоит рядом.';
 } else {
  s+=' помогает переносить мёртвую девушку, держа её за ноги и стараясь смотреть в сторону. Кровь из горла убитой капает прямо на пол, оставляя кровавый след.';
  if (current_location=='hall') {
    hall_blood=1;
    if (kover==0) kover_blood=1;
  }
  if (current_location=='bedroom') bedroom_blood=1;
  if (current_location=='bathroom') bathroom_blood=1;
 }

 $('#room_description').html(s);
 $('[data-toggle="tooltip"]').tooltip(); 
 if (current_location=='bathroom') $('#bathroom_link').html(`<a href='#' onclick="move('bathroom')">Ванная</a>`);
 if (current_location=='bedroom') $('#bedroom_link').html(`<a href='#' onclick="move('bedroom')">Спальня</a>`);
 if (current_location=='hall') $('#hall_link').html(`<a href='#' onclick="move('hall')">Гостиная</a>`);

 if (current_location=='bathroom') $('#bathroom_link_mob').html(`<a href='#' onclick="move('bathroom')">Ванная</a>`);
 if (current_location=='bedroom') $('#bedroom_link_mob').html(`<a href='#' onclick="move('bedroom')">Спальня</a>`);
 if (current_location=='hall') $('#hall_link_mob').html(`<a href='#' onclick="move('hall')">Гостиная</a>`);

 current_location=location;
 if (current_location=='bathroom') $('#bathroom_link').html('Ванная');
 if (current_location=='bedroom') $('#bedroom_link').html('Спальня');
 if (current_location=='hall') $('#hall_link').html('Гостиная');

 if (current_location=='bathroom') $('#bathroom_link_mob').html('Ванная');
 if (current_location=='bedroom') $('#bedroom_link_mob').html('Спальня');
 if (current_location=='hall') $('#hall_link_mob').html('Гостиная');
 inventory(); 

 $('.dropdown').mouseenter(function(){
    if(!$('.navbar-toggle').is(':visible')) { // disable for mobile view
        if(!$(this).hasClass('show')) { // Keeps it open when hover it again
          if (!touch_device)
            $('.dropdown-toggle-game', this).trigger('click');
        }
    }
 });

}

function refreshlinks() {
  $('#bathroom_link').html(`<a href='#' onclick="move('bathroom')">Ванная</a>`);
  $('#bedroom_link').html(`<a href='#' onclick="move('bedroom')">Спальня</a>`);
  $('#hall_link').html(`<a href='#' onclick="move('hall')">Гостиная</a>`);
  $('#bathroom_link_mob').html(`<a href='#' onclick="move('bathroom')">Ванная</a>`);
  $('#bedroom_link_mob').html(`<a href='#' onclick="move('bedroom')">Спальня</a>`);
  $('#hall_link_mob').html(`<a href='#' onclick="move('hall')">Гостиная</a>`);
  $('#move-counter').html(move_count);
}